﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WizardGeneric.Model;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using WizardGeneric.Helpers.Wizard;


namespace WizardGeneric.ViewModel
{
    public class WizardViewModel : IWizardViewModel
    {
		
        public List<WizardItem> WizardItems { get; set; } = new List<WizardItem>();

		public async Task ResetWizard()
		{
			await Task.Delay(0);
			WizardItems = WizardItems.AddMockData();
			WizardItems = WizardItems.ResetWizard();
		}
		public async Task NextStep()
		{
			await Task.Delay(0);
			WizardItems = WizardItems.NextStep();
		}
		public async Task PreviousStep()
		{
			await Task.Delay(0);
			WizardItems = WizardItems.PreviousStep();
		}
		public async Task NavigateTo(int ItemToSet)
		{
			await  Task.Delay(0);
			WizardItems = WizardItems.NavigateToClickedStep(ItemToSet);
		}
	}
}
