﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WizardGeneric.Model;

namespace WizardGeneric.ViewModel
{
	public interface IWizardViewModel
	{
            public List<WizardItem> WizardItems { get; set; }
			public Task NextStep();
			public Task PreviousStep();
			public Task NavigateTo(int ItemToSet);
			public Task ResetWizard();

	}
}
