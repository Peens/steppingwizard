﻿namespace WizardGeneric.Model
{
    public enum WizardEnum
    {
        ReadyForLaunch = 1,
        Launch = 2,
        SayHi = 3,
        Project = 4
    }
}