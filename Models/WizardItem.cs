﻿namespace WizardGeneric.Model
{
    public class WizardItem
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public bool IsActive { get; set; }
        public bool IsLastKnown { get; set; }
        public WizardEnum Identifier { get; set; }
        public string Src { get; set; }
    }

}