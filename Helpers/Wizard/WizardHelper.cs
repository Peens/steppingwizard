﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WizardGeneric.Model;

namespace WizardGeneric.Helpers.Wizard
{
	/// <summary>
	/// Generic Wizard Helper in order to create mutiple different wizards with different component styles
	/// </summary>
	public static class WizardHelper
	{
		/// <summary>
		///General Helper Method to ResetWixard
		/// </summary>
		public static  List<WizardItem> ResetWizard(this List<WizardItem> wizardlist)
		{
			wizardlist.SetActiveItem(0, true);
			wizardlist.SetLasyKnownItem(0, true);
			return wizardlist;
		}

		/// <summary>
		///General Helper Method to Get Active Items
		/// </summary>
		public static int GetLastKnownActiveItem(this List<WizardItem> wizardlist)
		{
			var activeItem = wizardlist.Where(n => n.IsActive == true).ToList().Count();
			return activeItem;
		}
		/// <summary>
		///General Helper Method to Get Inactive Items
		/// </summary>
		public static int GetLastKnownInActiveItem(this List<WizardItem> wizardlist)
		{
			var activeItem = wizardlist.Where(n => n.IsActive == false).ToList().Count();
			return activeItem;
		}
		/// <summary>
		///Item to Set As Active
		/// </summary>
		public static List<WizardItem> SetActiveItem(this List<WizardItem> wizardlist, int itemToSet, bool active)
		{
			wizardlist[itemToSet].IsActive = active;
			return wizardlist;
		}

		/// <summary>
		///Item to Set As LastKnown
		/// </summary>
		public static List<WizardItem> SetLasyKnownItem(this List<WizardItem> wizardlist, int itemToSet,bool isLastKnown)
		{
			wizardlist[itemToSet].IsLastKnown = isLastKnown;
			return wizardlist;
		}
		/// <summary>
		///Next Step In Wizard
		/// </summary>
		public static List<WizardItem> NextStep(this List<WizardItem> wizardlist)
		{
			var itemToSet = wizardlist.GetLastKnownActiveItem();
			wizardlist = wizardlist.ResetLastKnownItems(false);

			var maxpos = wizardlist.Count() - 1;
			if (itemToSet >= maxpos)
			{
				itemToSet = maxpos;
			}
			wizardlist = wizardlist.SetLasyKnownItem(itemToSet, true);
			wizardlist = wizardlist.SetActiveItem(itemToSet, true);
			return wizardlist;
		}
		/// <summary>
		///Previous Step In Wizard
		/// </summary>
		public static List<WizardItem> PreviousStep(this List<WizardItem> wizardlist)
		{
			var itemToSet = wizardlist.GetLastKnownActiveItem()-1;
			wizardlist = wizardlist.ResetLastKnownItems(false);
			
			if (itemToSet <= 1) {
				itemToSet = 1;
			}
			
			var lastknowToSet = itemToSet - 1;
			if (lastknowToSet <= 0) {
				lastknowToSet = 0;
			}

			wizardlist = wizardlist.SetLasyKnownItem(lastknowToSet, true);
			wizardlist = wizardlist.SetActiveItem(itemToSet, false);
			return wizardlist;
		}
		/// <summary>
		/// Set LastKnow Items to bool value
		/// </summary>
		public static List<WizardItem> ResetLastKnownItems(this List<WizardItem> wizardlist, bool lastKnown)
		{
			foreach (var item in wizardlist)
			{
				item.IsLastKnown = lastKnown;
			}
			return wizardlist;
		}

		/// <summary>
		/// Set AllActiveItems to value
		/// </summary>
		public static List<WizardItem> SetAllActiveItems(this List<WizardItem> wizardlist, bool active)
		{
			foreach (var item in wizardlist)
			{
				item.IsActive = active;
				item.IsLastKnown = false;
			}
			return wizardlist;
		}
		/// <summary>
		/// NavigateToASpecificStep
		/// </summary>
		public static List<WizardItem> NavigateToClickedStep(this List<WizardItem> wizardlist, int itemClickedIndex)
		{
			//ToDo 
			return wizardlist;
		}
	}
}
