﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WizardGeneric.Model;

namespace WizardGeneric.Helpers.Wizard
{
	public static class WizardMockData
	{

		/// <summary>
		///This Method has been created for MockData only
		/// </summary>
		public static List<WizardItem> AddMockData(this List<WizardItem> wizardlist)
		{
			wizardlist = new List<WizardItem>();

			var w1 = new WizardItem()
			{
				IsActive = true,
				Subtitle = "0",
				Title = "ReadyForLaunch",
				Identifier = WizardEnum.ReadyForLaunch,
				Src = "/images/1.gif"
			};
			var w2 = new WizardItem()
			{
				IsActive = false,
				Subtitle = "1",
				Title = "Launch",
				Identifier = WizardEnum.Launch,
				Src = "/images/2.gif"
			};
			var w3 = new WizardItem()
			{
				IsActive = false,
				Subtitle = "2",
				Title = "SayHi",
				Identifier = WizardEnum.SayHi,
				Src = "/images/3.gif"
			};

			var w4 = new WizardItem()
			{
				IsActive = false,
				Subtitle = "3",
				Title = "Project",
				Identifier = WizardEnum.Project,
				Src = "/images/4.gif"
			};
			wizardlist.Add(w1);
			wizardlist.Add(w2);
			wizardlist.Add(w3);
			wizardlist.Add(w4);
			return wizardlist;
		}
		
	}
}
