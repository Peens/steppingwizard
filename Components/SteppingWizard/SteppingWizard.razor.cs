using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using WizardGeneric.Model;
using WizardGeneric.ViewModel;

namespace WizardGeneric.Components
{
	public partial class WizardComponent:ComponentBase
	{

		/*Blazor Specific Events are Handled in the code behind ViewModels should handle te the rest*/
		[Inject]
		public IWizardViewModel WizardViewModel { get; set; }

		protected override void OnInitialized()
		{
			WizardViewModel.ResetWizard();
			base.OnInitialized();
		}
	}
}